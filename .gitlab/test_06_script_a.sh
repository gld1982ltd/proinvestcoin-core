#!/usr/bin/env bash
#
# Copyright (c) 2018 The Bitcoin Core developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

export LC_ALL=C.UTF-8

TRAVIS_COMMIT_LOG=$(git log --format=fuller -1)
export TRAVIS_COMMIT_LOG

OUTDIR=$BASE_OUTDIR/$CI_MERGE_REQUEST_ID/$CI_JOB_ID-$HOST
BITCOIN_CONFIG_ALL="--disable-dependency-tracking --prefix=$CI_PROJECT_DIR/depends/$HOST --bindir=$OUTDIR/bin --libdir=$OUTDIR/lib"
if [ -z "$NO_DEPENDS" ]; then
  CI_EXEC ccache --max-size=$CCACHE_SIZE
fi

if [ -n "$CONFIG_SHELL" ]; then
  CI_EXEC "$CONFIG_SHELL" -c "./autogen.sh"
else
  CI_EXEC ./autogen.sh
fi

mkdir build
cd build || (echo "could not enter build directory"; exit 1)

CI_EXEC ../configure --cache-file=config.cache $BITCOIN_CONFIG_ALL $BITCOIN_CONFIG || ( cat config.log && false)

CI_EXEC make distdir VERSION=$HOST

cd "proinvestcoin-$HOST" || (echo "could not enter distdir proinvestcoin-$HOST"; exit 1)

CI_EXEC ./configure --cache-file=../config.cache $BITCOIN_CONFIG_ALL $BITCOIN_CONFIG || ( cat config.log && false)

set -o errtrace
trap 'CI_EXEC "cat ${CI_PROJECT_DIR}/sanitizer-output/* 2> /dev/null"' ERR

CI_EXEC make $MAKEJOBS $GOAL || ( echo "Build failure. Verbose build follows." && CI_EXEC make $GOAL V=1 ; false )

cd ${CI_PROJECT_DIR} || (echo "could not enter gitlab build dir $CI_PROJECT_DIR"; exit 1)
