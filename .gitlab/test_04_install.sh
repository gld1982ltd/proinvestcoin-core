#!/usr/bin/env bash
#
# Copyright (c) 2018 The Bitcoin Core developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

export LC_ALL=C.UTF-8

CI_EXEC () {
  bash -c "cd $PWD && $*"
}

CI_EXEC apt-get update
CI_EXEC apt-get install --no-install-recommends --no-upgrade -qq $PACKAGES $CI_PACKAGES

export DIR_FUZZ_IN=${CI_PROJECT_DIR}/qa-assets
git clone https://github.com/bitcoin-core/qa-assets ${DIR_FUZZ_IN}
export DIR_FUZZ_IN=${DIR_FUZZ_IN}/fuzz_seed_corpus/

mkdir -p "${CI_PROJECT_DIR}/sanitizer-output/"
export ASAN_OPTIONS=""
export LSAN_OPTIONS="suppressions=${CI_PROJECT_DIR}/test/sanitizer_suppressions/lsan"
export TSAN_OPTIONS="suppressions=${CI_PROJECT_DIR}/test/sanitizer_suppressions/tsan:log_path=${CI_PROJECT_DIR}/sanitizer-output/tsan"
export UBSAN_OPTIONS="suppressions=${CI_PROJECT_DIR}/test/sanitizer_suppressions/ubsan:print_stacktrace=1:halt_on_error=1"
env | grep -E '^(BITCOIN_CONFIG|CCACHE_|WINEDEBUG|LC_ALL|BOOST_TEST_RANDOM|CONFIG_SHELL|(ASAN|LSAN|TSAN|UBSAN)_OPTIONS)' | tee /tmp/env
