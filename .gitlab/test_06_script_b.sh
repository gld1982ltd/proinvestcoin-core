#!/usr/bin/env bash
#
# Copyright (c) 2018 The Bitcoin Core developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

export LC_ALL=C.UTF-8

cd "build/proinvestcoin-$HOST" || (echo "could not enter distdir build/proinvestcoin-$HOST"; exit 1)

if [ "$RUN_UNIT_TESTS" = "true" ]; then
  CI_EXEC LD_LIBRARY_PATH=$CI_PROJECT_DIR/depends/$HOST/lib make $MAKEJOBS check VERBOSE=1
fi

if [ $((`date +%s`-$START_TIME)) -gt $RUN_TESTS_TIMEOUT ]; then
  RUN_FUNCTIONAL_TESTS=false;
fi

echo $((`date +%s`-$START_TIME))
echo $RUN_TESTS_TIMEOUT
echo "$RUN_FUNCTIONAL_TESTS"

if [ "$RUN_FUNCTIONAL_TESTS" = "true" ]; then
  CI_EXEC test/functional/test_runner.py --ci --combinedlogslen=4000 ${TEST_RUNNER_EXTRA} --quiet --failfast --proinvestcoin --insight --bitcoin
fi

if [ "$RUN_FUZZ_TESTS" = "true" ]; then
  CI_EXEC test/fuzz/test_runner.py -l DEBUG ${DIR_FUZZ_IN}
fi
